package com.codetaco.iot;

import com.codetaco.iot.ai.command.fromAI.ConfigureThingCommand;
import com.codetaco.iot.ai.kb.AI;
import com.codetaco.iot.ai.kb.Iot;
import com.codetaco.iot.ai.model.Component;
import com.codetaco.iot.ai.model.ComponentPin;
import com.codetaco.iot.ai.model.GpioPin;
import com.codetaco.iot.ai.model.PinId;
import com.codetaco.iot.ai.model.Thing;
import com.codetaco.iot.ai.model.ThingStatus;
import lombok.extern.slf4j.Slf4j;
import org.drools.core.impl.InternalKnowledgeBase;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.junit.jupiter.api.Test;
import org.kie.api.definition.KiePackage;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@Slf4j
public class BaseDroolsTests {

    private Iot iot;
    private AI ai;

    @Test
    void newThingIsReady() {
        KieSession knowledgeSession = session("system/ThingManagement.drl");

        iot = mock(Iot.class);
        Thing testThing = thing().build();

        knowledgeSession.insert(testThing);
        knowledgeSession.insert(iot);

        knowledgeSession.fireAllRules();
        assertEquals(ThingStatus.DISCONNECTED, testThing.getStatus());
        verify(iot).prepare(testThing);
    }

    @Test
    void sendConfigurationToAThing() {
        KieSession knowledgeSession = session("system/ThingManagement.drl");

        ai = mock(AI.class);
        Thing testThing = thing().status(ThingStatus.CONNECTED).build();

        knowledgeSession.insert(testThing);
        knowledgeSession.insert(ai);

        knowledgeSession.fireAllRules();
        assertEquals(ThingStatus.RUNNING, testThing.getStatus());
        verify(ai).send(anyString(), same(testThing), any(ConfigureThingCommand.class));
    }

    private Thing.ThingBuilder thing() {
        return Thing.builder()
                 .thingId("thingOne")
                 .component(Component.builder()
                              .componentId("led2Color")
                              .type("analogLed2Color")
                              .pin(ComponentPin.builder()
                                     .pinId(PinId.COLOR1)
                                     .pin(GpioPin._24)
                                     .build())
                              .pin(ComponentPin.builder()
                                     .pinId(PinId.COLOR2)
                                     .pin(GpioPin._1)
                                     .build())
                              .build());
    }

    private KieSession session(String classPathFile) {
        Resource ruleResource = ResourceFactory.newClassPathResource(classPathFile);
        InternalKnowledgeBase kb = KnowledgeBaseFactory.newKnowledgeBase();

        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(ruleResource, ResourceType.DRL);

        if (kbuilder.hasErrors()) {
            kbuilder.getErrors().stream()
              .map(KnowledgeBuilderError::toString)
              .forEach(log::error);
            fail();
        }
        Collection<KiePackage> pkgs = kbuilder.getKnowledgePackages();
        if (pkgs != null) {
            kb.addPackages(pkgs);
        }
        return kb.newKieSession();
    }

}
