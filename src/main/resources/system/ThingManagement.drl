package ThingManagement

import com.codetaco.iot.ai.kb.AI;
import com.codetaco.iot.ai.kb.Iot;
import com.codetaco.iot.ai.model.Thing;
import com.codetaco.iot.ai.model.ThingStatus;
import com.codetaco.iot.ai.model.EventType;
import com.codetaco.iot.ai.model.Component;
import com.codetaco.iot.ai.model.ComponentStatus;
import com.codetaco.iot.ai.command.fromAI.ConfigureThingCommand;
import com.codetaco.iot.ai.command.fromAI.ThingRemoveCommand;
import com.codetaco.iot.ai.command.fromAI.activity.Activity;
import com.codetaco.iot.ai.receiver.model.EventFromThing;
import com.codetaco.iot.ai.receiver.model.ComponentInfoFromThing;

global org.slf4j.Logger log;

rule "New thing is ready"
    dialect "java"
    salience -100
    when
        iot: Iot()
        thing: Thing(status == null)
    then
        iot.prepare(thing);
        modify(thing) {
            setStatus(ThingStatus.DISCONNECTED)
        }
end

rule "Send configuration to a thing"
    dialect "java"
    salience -100
    when
        thing : Thing(status == ThingStatus.CONNECTED)
        ai: AI()
    then
        modify(thing) {
            setStatus(ThingStatus.RUNNING)
        }
        ai.send("Send configuration to a thing", thing,
            ConfigureThingCommand.builder().build());
end

rule "insert component"
    dialect "java"
    salience -100
    when
        thing: Thing(status == ThingStatus.RUNNING, components : components)
        component: Component(newComponentId: componentId) from components
        not Component(componentId == newComponentId)
    then
        insert(component);
end

rule "thing is configured event"
    dialect "java"
    salience -100
    when
        event: EventFromThing(eventHeader : header,
            eventHeader.getType() == EventType.CONFIGURED,
            componentInfos : componentInfos)
        ci: ComponentInfoFromThing () from componentInfos
        component: Component(thingId == eventHeader.thingId
            && componentId == ci.componentId
            && status != ComponentStatus.CONFIGURED)
    then
        modify(component) {
            setStatus(ComponentStatus.CONFIGURED)
        }
        retract(event);
end

rule "ready to start"
    dialect "java"
    salience -100
    when
        component: Component(status == ComponentStatus.CONFIGURED)
    then
        modify (component) {
            setStatus(ComponentStatus.START)
        }
end

rule "component should be started"
    dialect "java"
    salience -100
    when
        ai: AI()
        component: Component(status == ComponentStatus.START, thingId : thingId)
        thing: Thing(thingId == thingId)
    then
        modify(component) {
            setStatus(ComponentStatus.STARTING)
        }
        ai.send("component should be started", thing,
            Activity.builder()
                .type("start")
                .componentId(component.getComponentId())
                .build());
end

rule "component has started event"
    dialect "java"
    salience -100
    when
        event: EventFromThing(eventHeader: header,
            eventHeader.getType() == EventType.STARTED,
            componentInfos : componentInfos)
        ci: ComponentInfoFromThing () from componentInfos
        component: Component(thingId == eventHeader.thingId && componentId == ci.componentId)
    then
        modify(component) {
            setStatus(ComponentStatus.STARTED)
        }
        retract(event)
end

rule "component is now running"
    dialect "java"
    salience -100
    when
        component: Component(status == ComponentStatus.STARTED)
    then
        modify(component) {
            setStatus(ComponentStatus.RUNNING)
        }
end

rule "thing stop event has arrived - stopping thing"
    dialect "java"
    when
        ai: AI()
        event: EventFromThing(eventHeader: header,
            eventHeader.getType() == EventType.STOP)
        thing: Thing(thingId == eventHeader.thingId)
    then
        modify(thing){
            setStatus(ThingStatus.STOPPING)
        }
    end

rule "thing has stopped"
    dialect "java"
    salience -100
    when
        ai: AI()
        thing: Thing(thingId == thingId && status == ThingStatus.STOPPING)
        not Component (thingId == thingId && status == ComponentStatus.RUNNING)
    then
        modify(thing){
            setStatus(ThingStatus.STOPPED)
        }
    end

rule "thing is retracted and removed"
    dialect "java"
    when
        ai:AI()
        event: EventFromThing(eventHeader: header,
            eventHeader.getType() == EventType.STOP)
        thing: Thing(thingId == eventHeader.thingId && status == ThingStatus.STOPPED)
        not Component(thingId == eventHeader.thingId)
    then
        ai.send("thing is retracted and removed", thing,
            ThingRemoveCommand.builder().build());
        retract(event)
        retract(thing)
    end

rule "thing stop event has arrived - stopping components"
    dialect "java"
    salience -100
    when
        ai: AI()
        event: EventFromThing(eventHeader: header,
            eventHeader.getType() == EventType.STOP)
        component: Component (thingId == eventHeader.thingId
            && status == ComponentStatus.RUNNING)
    then
        modify(component){
            setStatus(ComponentStatus.STOPPING)
        }
    end

rule "component stopped"
    dialect "java"
    salience -100
    when
        ai: AI()
        component: Component (thingId == thingId && status == ComponentStatus.STOPPING)
        thing: Thing(thingId == thingId)
    then
        modify(component){
            setStatus(ComponentStatus.STOPPED)
        }
        ai.send("component stopped", thing,
            Activity.builder()
                .type("stop")
                .componentId(component.getComponentId())
                .build());
    end

rule "component retracted"
    dialect "java"
    salience -100
    when
        ai: AI()
        event: EventFromThing(eventHeader: header,
            eventHeader.getType() == EventType.STOP)
        component: Component (thingId == eventHeader.thingId
            && status == ComponentStatus.STOPPED)
    then
        retract(component)
    end
